# Arch Repo

Open public repository built and hosted in the public on [gitlab.com](https://gitlab.com/).

Everything is in public repos and built with Gitlab CI where you can see the public CI job that builds and adds the package to the repo.

## Quick install

Add this to your `/etc/pacman.conf`

```
[archrepo]
SigLevel = Optional
Server = https://arch-repo.gitlab.io/arch-repo
```

## Repository

This repo contains the repository which is hosted at [https://arch-repo.gitlab.io/arch-repo](https://arch-repo.gitlab.io/arch-repo) with GitLab Pages.

## Packages

Package build scripts are at [https://gitlab.com/arch-repo/packages](https://gitlab.com/arch-repo/packages).

Each one is a CI build script which uses the [scripts](https://gitlab.com/arch-repo/scripts) to build the package and add it to the repository.
